{{--*/ $twitter = Session::get('access_token') /*--}}


@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
			@if(isset($data->profile_banner_url))
					<img class="screen" src="{{$data->profile_banner_url}}">
			@endif
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<img class="img-thumbnail avatar" src="{{$data->profile_image_url}}">
								<h3><a href="http://twitter.com/{{$data->screen_name}}">{{$data->name}}</a></h3>
								<p><a href="http://twitter.com/{{$data->screen_name}}">{{$data->screen_name}}</a></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<h4><a href="{{url('twitter/user/'.$data->screen_name)}}">Tweet</a></h4>
								<p>{{$data->statuses_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/follower/'.$data->screen_name)}}">Follower</a></h4>
								<p>{{$data->followers_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/following/'.$data->screen_name)}}">Following</a></h4>
								<p>{{$data->friends_count}}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control status" placeholder="what happening... " rows="5"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStatus">Send</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control story" placeholder="tweet story... " rows="9"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStory">Send Story</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">Log</div>
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<div class="log"></div>
								<br>
								<button class="btn btn-danger btn-clearLog">clear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="margin-none">Mentions</h3></div>
					<div class="panel-body">
						<ul class="list_status">
							@foreach($status as $s)
							<?php
								$text = null;
								if(isset($s->text)){
									$text = $s->text;
								}
							?>
								<li> <img class="img-thumbnail avatar" src="{{$s->user->profile_image_url}}"> 
								<h4 class="float-left"><a href="{{url('twitter/follower/'.$s->user->screen_name)}}">{{$s->user->name}}</a></h4>
								<h5 class="float-left"><a target="_blank" href="http://twitter.com/{{$s->user->screen_name}}">{{'@'.$s->user->screen_name}}</a></h5>
								<small class="float-left"><a target="_blank" href="https://twitter.com/{{$s->user->screen_name}}/status/{{$s->id_str}}">{{Twitter::ago($s->created_at)}}</a></small>
								<br>
								<p>{!! \CustomLib::autoURLtwitter($text) !!}</p>
								<small class="status-tool"><span class="retweet opacity50" data-id="{{$s->id_str}}" data-status="{{$text}}"><i class="fa fa-retweet"></i></span> <span class="reply opacity50" data-id="{{$s->id_str}}" data-user="{{$s->user->screen_name}}" data-status="{{$text}}"><i class="fa fa-reply"></i></span></small>
								
								</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('modal')
@endsection

