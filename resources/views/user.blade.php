{{--*/ $twitter = Session::get('access_token') /*--}}


@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
			@if(isset($data->profile_banner_url))
					<img class="screen" src="{{$data->profile_banner_url}}">
			@endif
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<img class="img-thumbnail avatar" src="{{$data->profile_image_url}}">
								<h3><a href="http://twitter.com/{{$data->screen_name}}">{{$data->name}}</a></h3>
								<p><a href="http://twitter.com/{{$data->screen_name}}">{{$data->screen_name}}</a></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3">
								<h4><a href="{{url('twitter/user/'.$data->screen_name)}}">Tweet</a></h4>
								<p>{{$data->statuses_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/follower/'.$data->screen_name)}}">Follower</a></h4>
								<p>{{$data->followers_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/following/'.$data->screen_name)}}">Following</a></h4>
								<p>{{$data->friends_count}}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control status" placeholder="what happening... " rows="5"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStatus">Send</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control story" placeholder="tweet story... " rows="9"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStory">Send Story</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">Log</div>
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<div class="log"></div>
								<br>
								<button class="btn btn-danger btn-clearLog">clear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
			<div class="row">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingOne">
					      <h3 class="margin-none">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          Tool
					        </a>
					      </h3>
					    </div>
					    <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
					      <div class="panel-body">
					        <div class="col-md-3">
					        	<input type="radio" name="radio_select" value="select" checked="checked"> Select 
					        	<br>
					        	<input type="radio" name="radio_select"  value="unselect"> Unselect
					        </div>
					        <div class="col-md-3">
					        	<input type="radio" name="radio_follow" value="all" checked="checked"> All 
					        	<br>
					        	<input type="radio" name="radio_follow"  value="followed"> Followed
					        	<br>
					        	<input type="radio" name="radio_follow"  value="not_followed"> Not Followed
					        	<br>
					        	<input type="radio" name="radio_follow"  value="following"> Following
					        	<br>
					        	<input type="radio" name="radio_follow"  value="not_following"> Not Following
					        </div>
					        <div class="col-md-3">
					        	<input type="radio" name="radio_time" value="all" checked="checked"> All 
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1hour"> +1 hour
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1hour"> -1 hour
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1day"> +1 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1day"> -1 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1week"> +1 week
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1week"> -1 week
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1month"> +1 month
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1month"> -1 month
					        </div>
					        <div class="col-md-3">
					        	<button class="btn btn-select btn-block btn-danger">GO</button>
					        	<button class="btn  btn-block btn-default tool_unselect_all">Reset</button>
					        	<button class="btn  btn-block btn-success tool_retweet"><i class="fa fa-retweet "></i>  Retweet</button>
					        	<button class="btn  btn-block btn-primary tool_follow"><i class="fa fa-user "></i>  Follow</button>
					        	<button class="btn  btn-block btn-primary tool_unfollow"><i class="fa fa-user-times "></i> Unfollow</button>
					        	<button class="btn  btn-block btn-success tool_check_friend"><i class="fa fa-users "></i> Check Friend</button>
					        </div>
					      </div>
					    </div>
					  </div>
					  
				
					</div>
			</div>
			<div class="row">
				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="margin-none">Time Line <small><a href="{{url('twitter/user/'.$data->screen_name).'?view=home'}}">Home</a> | <a href="{{url('twitter/user/'.$data->screen_name)}}">Status</a></small></h3></div>
					<div class="panel-body">
						<ul class="list_status">
							@foreach($status as $s)
							<?php

								$followed = 0;
								$following = 0;
								$timelapse = 86400*365;
								$text = null;
								if(isset($s->text)){
									$text = $s->text;
									$timelapse = \CustomLib::difference($s->created_at);

								}
						
							?>
								<li data-id="{{$s->user->id_str}}"> <img class="img-thumbnail avatar" src="{{$s->user->profile_image_url}}"> 
								<h4 class="float-left"><a href="{{url('twitter/follower/'.$s->user->screen_name)}}">{{$s->user->name}}</a></h4>
								<h5 class="float-left"><a target="_blank" href="http://twitter.com/{{$s->user->screen_name}}">{{'@'.$s->user->screen_name}}</a></h5>
								<small class="float-left"><a target="_blank" href="https://twitter.com/{{$s->user->screen_name}}/status/{{$s->id_str}}">{{Twitter::ago($s->created_at)}}</a></small>
								<br>
								<p>{!! \CustomLib::autoURLtwitter($text) !!}</p>
								<small class="status-tool"><span class="retweet opacity50" data-id="{{$s->id_str}}" data-status="{{$text}}"><i class="fa fa-retweet"></i></span> <span class="reply opacity50" data-id="{{$s->id_str}}" data-user="{{$s->user->screen_name}}" data-status="{{$text}}"><i class="fa fa-reply"></i></span></small>
								<input type="checkbox" class="cb_user" data-followed="{{$followed}}"  data-following="{{$following}}" data-id="{{$s->user->id_str}}" data-user="{{$s->user->screen_name}}" data-timelapse="{{$timelapse}}" data-status="{{$text}}" data-id-status="{{$s->id_str}}"/>
								</li>
							@endforeach
						</ul>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('modal')
@endsection

