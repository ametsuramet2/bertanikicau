{{--*/ $twitter = Twitter::getCredentials()/*--}}
{{--*/ $segment = \Request::segment(2); /*--}}


@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default">
			@if(isset($data->profile_banner_url))
					<img class="screen" src="{{$data->profile_banner_url}}">
			@endif
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<img class="img-thumbnail avatar" src="{{$data->profile_image_url}}">
								<h3><a href="http://twitter.com/{{$data->screen_name}}">{{$data->name}}</a></h3>
								<p><a href="http://twitter.com/{{$data->screen_name}}">{{$data->screen_name}}</a></p>
							</div>
						</div> 
						<div class="row">
							<div class="col-md-3">
								<h4><a href="{{url('twitter/user/'.$data->screen_name)}}">Tweet</a></h4>
								<p>{{$data->statuses_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/follower/'.$data->screen_name)}}">Follower</a></h4>
								<p>{{$data->followers_count}}</p>
							</div>
							<div class="col-md-3">
								<h4><a href="{{url('twitter/following/'.$data->screen_name)}}">Following</a></h4>
								<p>{{$data->friends_count}}</p>
							</div>
						</div>
						<div class="row">
						<div class="col-md-12">
							{{--*/ $mention = "" /*--}}
							@if($data->screen_name!=$twitter->screen_name)
								<?php 
								$user_lookup = Twitter::getFriendshipsLookup(['user_id'=>$data->id_str]);
								$user_followed = 0;
								$user_following = 0;
								if (in_array("followed_by", $user_lookup[0]->connections)) {
								    $user_followed = 1;
								}
								if (in_array("following", $user_lookup[0]->connections)) {
								    $user_following = 1;
								}
								$mention = '@'.$data->screen_name.' ';
								// print_r($user_lookup)
								?>
								@if($user_followed)
								<span class="badge alert-success"><i class="fa fa-check-square-o"></i> FOLLOWS YOU</span>
				        		@endif
				        		@if($user_following)
				        		<span class="badge alert-danger"><i class="fa fa-check-square-o"></i> FOLLOWING</span>
				        		<br>
				        		<br>
				        		<button data-id="{{$data->id_str}}"  data-user="{{$data->screen_name}}" class="btn  btn-block btn-primary btn_unfollow_user"><i class="fa fa-user-times "></i> Unfollow {{$data->screen_name}}</button>
				        		@else
				        		<br>
				        		<br>
				        		<button data-id="{{$data->id_str}}"  data-user="{{$data->screen_name}}" class="btn  btn-block btn-primary btn_follow_user"><i class="fa fa-user "></i> Follow {{$data->screen_name}}</button>
				        		@endif
							@endif
						</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control status" placeholder="what happening... " rows="5">{{$mention}}</textarea>
								<br>
								<button class="btn btn-danger btn-updateStatus">Send</button>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control story" placeholder="tweet story... " rows="9"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStory">Send Story</button>
							</div>
						</div>
					</div>
				</div>
				@if($segment=="follower" && $data->screen_name==$twitter->screen_name)
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control DM_brodcast" placeholder="seperate with pipeline ( | )... " rows="5"></textarea>
								<br>
								<button class="btn btn-danger btn-DMBroadcast"><i class="fa fa-envelope"></i> Send Direct Message</button>
							</div>
						</div>
					</div>
				</div>
				@endif
				<div class="panel panel-default">
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<textarea class="form-control status_brodcast" placeholder="seperate with pipeline ( | )... " rows="5"></textarea>
								<br>
								<button class="btn btn-danger btn-updateStatusBroadcast">Send Broadcast</button>
							</div>
						</div>
					</div>
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">Log</div>
					<div class="panel-body panel-user">
						<div class="row">
							<div class="col-md-12">
								<div class="log"></div>
								<br>
								<button class="btn btn-danger btn-clearLog">clear</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8">
			<div class="row">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  <div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingOne">
					      <h3 class="margin-none">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
					          Tool
					        </a>
					      </h3>
					    </div>
					    <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
					      <div class="panel-body">
					        <div class="col-md-3">
					        	<input type="radio" name="radio_select" value="select" checked="checked"> Select 
					        	<br>
					        	<input type="radio" name="radio_select"  value="unselect"> Unselect
					        </div>
					        <div class="col-md-3">
					        	<input type="radio" name="radio_follow" value="all" checked="checked"> All 
					        	<br>
					        	<input type="radio" name="radio_follow"  value="followed"> Followed
					        	<br>
					        	<input type="radio" name="radio_follow"  value="not_followed"> Not Followed
					        	<br>
					        	<input type="radio" name="radio_follow"  value="following"> Following
					        	<br>
					        	<input type="radio" name="radio_follow"  value="not_following"> Not Following
					        </div>
					        <div class="col-md-3">
					        	<input type="radio" name="radio_time" value="all" checked="checked"> All 
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1hour"> +1 hour
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1hour"> -1 hour
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1day"> +1 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1day"> -1 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="+2day"> +2 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="-2day"> -2 day
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1week"> +1 week
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1week"> -1 week
					        	<br>
					        	<input type="radio" name="radio_time"  value="+1month"> +1 month
					        	<br>
					        	<input type="radio" name="radio_time"  value="-1month"> -1 month
					        </div>
					        <div class="col-md-3">
					        	<button class="btn btn-select btn-block btn-danger">GO</button>
					        	<button class="btn  btn-block btn-default tool_unselect_all">Reset</button>
					        	<button class="btn  btn-block btn-success tool_retweet"><i class="fa fa-retweet "></i>  Retweet</button>
					        	<button class="btn  btn-block btn-primary tool_follow"><i class="fa fa-user "></i>  Follow</button>
					        	<button class="btn  btn-block btn-primary tool_unfollow"><i class="fa fa-user-times "></i> Unfollow</button>
					        </div>
					      </div>
					    </div>
					  </div>
					  
				
					</div>
			</div>
			<div class="row">
				<div class="panel panel-default">
					    <div class="panel-heading" role="tab" id="headingTwo">
					   
					      <h3 class="margin-none">
					      	
					      Time Line <small><a href="{{url('twitter/user/'.$data->screen_name).'?view=home'}}">Home</a> | <a href="{{url('twitter/user/'.$data->screen_name)}}">Status</a></small>
								<div class="pull-right">
									@if($friend->previous_cursor_str)
									<a class="btn btn-default" href="?cursor={{$friend->previous_cursor_str}}">
										<i class="fa  fa-backward"></i>
									</a>
									@endif
									@if($friend->next_cursor_str)
									<a class="btn btn-default" href="?cursor={{$friend->next_cursor_str}}">
										<i class="fa  fa-forward"></i>
									</a>
									@endif
									
								</div>
							</h3>
					    </div>
					    
					      <div class="panel-body">
								<ul class="list_status">
									@foreach($friend->users as $i=>$s)
									<?php
										$followed = 0;
										$following = 0;
										$timelapse = 86400*365;
										$status = null;
										if(isset($lookup[$i])){
											$connections = $lookup[$i]->connections;
											if (in_array("followed_by", $connections)) {
											    $followed = 1;
											}
											if (in_array("following", $connections)) {
											    $following = 1;
											}
	
											
											if(isset($s->status->text)){
												$status = $s->status->text;
												$timelapse = \CustomLib::difference($s->status->created_at);
											}
										}


									?>
										<li> <img class="img-thumbnail avatar" src="{{$s->profile_image_url}}"> 
										<h4 class="float-left"><a href="{{url('twitter/follower/'.$s->screen_name)}}">{{$s->name}}</a></h4>
										<h5 class="float-left"><a target="_blank" href="http://twitter.com/{{$s->screen_name}}">{{'@'.$s->screen_name}}</a></h5>
										@if(isset($s->status))
										<small class="float-left"><a target="_blank" href="https://twitter.com/{{$s->screen_name}}/status/{{$s->status->id_str}}">{{Twitter::ago($s->status->created_at)}}</a></small>
										<br>

										<p>{!! \CustomLib::autoURLtwitter($status) !!}</p>
										<small class="status-tool"><span class="retweet opacity50" data-id="{{$s->status->id_str}}" data-status="{{$status}}"><i class="fa fa-retweet"></i></span> <span class="reply opacity50" data-id="{{$s->status->id_str}}" data-user="{{$s->screen_name}}" data-status="{{$status}}"><i class="fa fa-reply"></i></span></small>
										<div class="clear"></div>
										

										@endif
										<div class="friend-status">
											@if($followed)
											<span class="badge alert-success"><i class="fa fa-check-square-o"></i> FOLLOWS YOU</span>
							        		
							        		@endif
							        		@if($following)
							        		<span class="badge alert-danger"><i class="fa fa-check-square-o"></i> FOLLOWING</span>
							        		
							        		@endif
										</div>
										<input type="checkbox" class="cb_user" data-followed="{{$followed}}"  data-following="{{$following}}" data-id="{{$s->id_str}}" data-user="{{$s->screen_name}}" data-timelapse="{{$timelapse}}" data-status="{{$status}}" data-id-status="{{$s->id_str}}"/>
										</li>
									@endforeach
								</ul>
							</div>
					  </div>
			</div>
					{{-- <div class="row">
					<div class="col-md-3 pull-right">

						<ul class="nav navbar-nav navbar-right">
						<li class="dropdown tool">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Tools <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a class="tool_select_all" href="#"><i class="fa fa-check-square"></i> Select All</a></li>
								<li><a class="tool_select_followed" href="#"><i class="fa fa-check-square"></i> Select Followed</a></li>
								<li><a class="tool_select_not_followed" href="#"><i class="fa fa-check-square"></i> Select Not Followed</a></li>
								<li><a class="tool_select_following" href="#"><i class="fa fa-check-square"></i> Select Following</a></li>
								<li><a class="tool_select_not_following" href="#"><i class="fa fa-check-square"></i> Select Not Following</a></li>
								<li><a class="tool_select_below_oneday" href="#"><i class="fa fa-check-square"></i> Select -1 day</a></li>
								<li><a class="tool_select_oneday" href="#"><i class="fa fa-check-square"></i> Select +1 day</a></li>
								<li><a class="tool_select_oneweek" href="#"><i class="fa fa-check-square"></i> Select +1 week</a></li>
								<li><a class="tool_select_onemonth" href="#"><i class="fa fa-check-square"></i> Select +1 month</a></li>
								<li><a class="tool_unselect_all" href="#"><i class="fa fa-times-circle"></i> Unselect All</a></li>
								<li><a class="tool_unselect_below_oneday" href="#"><i class="fa fa-times-circle"></i> Unselect -1 day</a></li>
								<li><a class="tool_unselect_oneday" href="#"><i class="fa fa-times-circle"></i> Unselect +1 day</a></li>
								<li><a class="tool_unselect_oneweek" href="#"><i class="fa fa-times-circle"></i> Unselect +1 week</a></li>
								<li><a class="tool_unselect_onemonth" href="#"><i class="fa fa-times-circle"></i> Unselect +1 month</a></li>
								<li><a class="tool_toggle_select" href="#"><i class="fa fa-times-circle"></i> Toggle Select</a></li>
								<li><a class="tool_follow" href="#"><i class="fa fa-user "></i> Follow</a></li>
								<li><a class="tool_unfollow" href="#"><i class="fa fa-user-times"></i> Unfollow</a></li>
							</ul>
						</li>
						</ul>
					</div>
					</div> --}}
					{{-- <div class="row">
					<div class="col-md-12">

						
						</div> --}}
				</div>
			</div>
		</div>
	</div>
</div>
@include('modal')
@endsection

