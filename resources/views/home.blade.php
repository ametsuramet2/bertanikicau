{{--*/ $twitter = Session::get('access_token') /*--}}


@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<div class="text-center">
					@if($twitter)
						<a  class="btn btn-info" href="{{ url('/twitter/user/'.$twitter['screen_name']) }}"><i class="fa fa-twitter"></i> Connected to {{$twitter['screen_name'] }}</a>
					@else
						<a href="{{ url('twitter/login') }}" class="btn btn-info"><i class="fa fa-twitter"></i> Connect to twitter</a>
					@endif

					
				
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection



