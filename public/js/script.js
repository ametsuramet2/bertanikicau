var time = 2000

$('.btn-select').click(function(){
	var select = $('[name=radio_select]:checked').val()
	var follow = $('[name=radio_follow]:checked').val()
	var time = $('[name=radio_time]:checked').val()
	// alert(select+' '+follow+' '+time)
	var select = (select=="select")?true:false;
	// $('.cb_user').each(function(i,item){
		switch(follow){
			case 'all' :
				var selector = $('.cb_user')
			break;
			case 'followed' :
				var selector = $('.cb_user[data-followed="1"]')
			break;
			case 'not_followed' :
				var selector = $('.cb_user[data-followed="0"]')
			break;
			case 'following' :
				var selector = $('.cb_user[data-following="1"]')
			break;
			case 'not_following' :
				var selector = $('.cb_user[data-following="0"]')
			break;
		}

		switch(time){
			case 'all' :
				selector.prop('checked',select)
				// console.log($this)
			break;
			case '+1hour' :
				selector.each(function(i,item){
					if($(this).data('timelapse')>3600){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '-1hour' :
				selector.each(function(i,item){
					if($(this).data('timelapse')<=3600){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '+1day' :
				selector.each(function(i,item){
					if($(this).data('timelapse')>86400){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '-1day' :
				selector.each(function(i,item){
					if($(this).data('timelapse')<=86400){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '+2day' :
				selector.each(function(i,item){
					if($(this).data('timelapse')>172800){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '-2day' :
				selector.each(function(i,item){
					if($(this).data('timelapse')<=172800){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '+1week' :
				selector.each(function(i,item){
					if($(this).data('timelapse')>604800){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '-1week' :
				selector.each(function(i,item){
					if(parseInt($(this).data('timelapse'))<604800){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '+1month' :
				selector.each(function(i,item){
					if($(this).data('timelapse')>2419200){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
			case '-1month' :
				selector.each(function(i,item){
					if($(this).data('timelapse')<=2419200){
						$(this).prop('checked',select)
					}else{
						$(this).prop('checked',!select)
					}
				})
			break;
		}
	// })
	var selected = $('.cb_user:checked').length
	$('.log').append('['+selected+' Selected]<br>');
	$('title').html('['+selected+' Selected]');

});	
$('.tool_toggle_select').click(function(){
	
	$('.cb_user').each(function(i,item){
		$(this).prop('checked',!$(this).prop("checked"))
	})
});	
$('.tool_select_all').click(function(){
	$('.cb_user').prop('checked',true)
});	
$('.tool_select_followed').click(function(){
	$('.cb_user[data-followed=1]').prop('checked',true)
});	
$('.tool_select_not_followed').click(function(){
	$('.cb_user[data-followed=0]').prop('checked',true)
});	

$('.tool_select_below_oneday').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')<86400){
			$(this).prop('checked',true)
		}
	})
});	
$('.tool_select_oneday').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>86400){
			$(this).prop('checked',true)
		}
	})
});	
$('.tool_select_oneweek').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>604800){
			$(this).prop('checked',true)
		}
	})
});	
$('.tool_select_onemonth').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>2419200){
			$(this).prop('checked',true)
		}
	})
});	

$('.tool_unselect_all').click(function(){
	$('.cb_user').prop('checked',false)
});	
$('.tool_unselect_oneday').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>86400){
			$(this).prop('checked',false)
		}
	})
});	
$('.tool_unselect_below_oneday').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')<86400){
			$(this).prop('checked',false)
		}
	})
});	
$('.tool_unselect_oneweek').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>604800){
			$(this).prop('checked',false)
		}
	})
});	
$('.tool_unselect_onemonth').click(function(){
	$('.cb_user').each(function(i,item){
		if($(this).data('timelapse')>2419200){
			$(this).prop('checked',false)
		}
	})
});	
$('.tool_select_following').click(function(){
	$('.cb_user[data-following=1]').prop('checked',true)
});	
$('.tool_select_not_following').click(function(){
	$('.cb_user[data-following=0]').prop('checked',true)
});	
$('.search').keypress(function(e){
	var q = $(this).val()
	if(e.charCode==13){
		location.href=base+'/twitter/search?q='+q
		return false
	}
});	
$('.btn_follow_user').click(function(){
	var id = $(this).data('id')
	var user = $(this).data('user')
	$.ajax({
				url :base+'/ajax/follow',
					data:{id:id,user:user},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    	
			  });
});
$('.btn_unfollow_user').click(function(){
	var id = $(this).data('id')
	var user = $(this).data('user')
	$.ajax({
				url :base+'/ajax/unfollow',
					data:{id:id,user:user},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    	
			  });
});
$('.tool_follow').click(function(){
	var row = []
	var user = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id'));
			user.push($(this).find('.cb_user').data('user'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var ok=0;
	var error=0;
	var count = row.length
	
	var start = setInterval( function(){
	    // console.log(row[i]);
	    if(i<count){
		    $.ajax({
				url :base+'/ajax/follow',
					data:{id:row[i],user:user[i]},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    	if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			  });
		}
		if(i>count){
			$('.log').append("[Finish] : Followed "+ok+" Users<br>");
			$('title').html("[Finish] : Followed "+ok+" Users<br>");
			clearInterval(start);
		}
		i++;
	}
	, time);
});	
$('.tool_retweet').click(function(){
	var row = []
	var status = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id-status'));
			status.push($(this).find('.cb_user').data('status'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var ok=0;
	var error=0;
	var count = row.length
	
	var start = setInterval( function(){
	    // console.log(row[i]);
	    if(i<count){
		    
			var data = {
				status : status[i],
				id : row[i],
			}
			$.ajax({
					url :base+'/ajax/retweetStatus',
					data:data,
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {
		    		$('.log').append(data.log);
				    $('title').html(data.log);
				    if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			  });
		}
		if(i>count){
			$('.log').append("[Finish] : Retweeted "+ok+" Users<br>");
			$('title').html("[Finish] : Retweeted "+ok+" Users<br>");
			clearInterval(start);
		}
		i++;
	}
	, time);
});	
$('.tool_unfollow').click(function(){
	var row = []
	var user = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id'));
			user.push($(this).find('.cb_user').data('user'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var ok=0;
	var error=0;
	var count = row.length
	
	var start = setInterval( function(){
	    // console.log(row[i]);
	    if(i<count){
		    $.ajax({
				url :base+'/ajax/unfollow',
					data:{id:row[i],user:user[i]},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    	if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			  });
		}
		if(i>count){
			$('.log').append("[Finish] : Unfollowed "+ok+" Users<br>");
			$('title').html("[Finish] : Unfollowed "+ok+" Users<br>");
			clearInterval(start);
		}
		i++;
	}
	, time);
});	
$('.retweet').click(function(){
	var $this = $(this)
	var id = $this.data('id')
	var status = $this.data('status')

	var data = {
				status : status,
				id : id,
			}
	$.ajax({
			url :base+'/ajax/retweetStatus',
			data:data,
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    }).done(function(data) {
    		$('.log').append(data.log);
		    $('title').html(data.log);
	  });
});
$('.reply').click(function(){
	var $this = $(this)
	var user = $this.data('user')
	var id = $this.data('id')
	var html = "<textarea class='form-control reply_status' rows=5>@"+user+" </textarea><br><small class='count_text'></small>"
	var footer = ' <button type="button" class="btn btn-primary send_reply">Send</button>'
	$('#modal1 .modal-header .modal-title').html("Reply to : @"+user)
	$('#modal1 .modal-body').html(html)
	$('#modal1 .modal-footer').html(footer)
	$('#modal1').modal("show")

	$('#modal1').on('shown.bs.modal', function (event) {
		// alert("aye")

		$('.reply_status').keyup(function(e) {
		    var tval = $(this).val(),
		        tlength = tval.length,
		        set = 140,
		        remain = parseInt(set - tlength);
		        $('.count_text').text(remain)
		    if (remain <= 0 && e.which !== 0 && e.keyCode !== 0) {
		        $(this).val((tval).substring(0, set-1))
		    }
		    // console.log(e)
		})

		$('.reply_status').bind('paste', function(e){ 
			var tval = $(this).val(),
		        tlength = tval.length,
		        set = 140,
		        remain = parseInt(set - tlength);
		        $('.count_text').text(remain)
		        $(this).val((tval).substring(0, set-1))
		});

		$('.send_reply').click(function(){
			var data = {
				status : $('.reply_status').val()
			}
			$.ajax({
				url :base+'/ajax/updateStatus',
				data:data,
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    }).done(function(data) {

		    		$('.log').append(data.log);
			    	$('title').html(data.log);
		    		$('.reply_status').val("");
		    		$('#modal1').modal('hide')
		  });
		})
	})
});
$('.btn-clearLog').click(function(){
	$('.log').html("")
});
$('.btn-updateStatus').click(function(){
	var data = {
		status : $('.status').val()
	}
	$.ajax({
		url :base+'/ajax/updateStatus',
		data:data,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }).done(function(data) {
    	if(data.status=="ok"){

    		$('.log').append(data.log);
			$('title').html(data.log);
    		$('.status').val("");
    	}else{
    		$('.log').append(data.log);
			$('title').html(data.log);
    	}
  });
})
$('.tool_check_friend').click(function(){
	var row = []
	var user = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id'));
			user.push($(this).find('.cb_user').data('user'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var count = row.length
	
	var status_fix = ""
	var ok = 0
	var error = 0
	var start = setInterval( function(){
	    // console.log(row[i]);
	    var status_random = status[Math.floor(Math.random() * status.length)];
	    status_fix = status_random
	    if(i<count){
	    	$.ajax({
					url :base+'/ajax/checkFriend',
					data:{id:row[i],user:user[i]},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {

			    	console.log(data);
			    	if(data.status=="ok"){
			    		if(inArray(['followed_by'], data.lookup)) {
						    $('.list_status li[data-id='+data.id+']').append('<span class="badge alert-success"><i class="fa fa-check-square-o"></i> FOLLOWS YOU</span>')
						    $('.cb_user[data-id='+data.id+']').attr('data-followed',1)
						}
						if(inArray(['following'], data.lookup)) {
						    $('.list_status li[data-id='+data.id+']').append('<span class="badge alert-danger"><i class="fa fa-check-square-o"></i> FOLLOWING</span>')
						    $('.cb_user[data-id='+data.id+']').attr('data-following',1)
							
						}
			    	
			    	}
			    	// 	$('.log').append(data.log);
			    	// 	$('title').html(data.log);
			    	
			    
			  });

		}
		// if(i>count){
		// 	$('.log').append("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
		// 	$('title').html("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
		// 	clearInterval(start);
		// }
		i++;
	}
	, time);
});
$('.btn-DMBroadcast').click(function(){
	var status_broadcast = $('.DM_brodcast').val()
	var status =  status_broadcast.split("|");

	var row = []
	var user = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id'));
			user.push($(this).find('.cb_user').data('user'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var count = row.length
	
	var status_fix = ""
	var ok = 0
	var error = 0
	var start = setInterval( function(){
	    // console.log(row[i]);
	    var status_random = status[Math.floor(Math.random() * status.length)];
	    status_fix = status_random
	    if(i<count){
	    	$.ajax({
					url :base+'/ajax/DM',
					data:{status:status_fix,id:row[i],user:user[i]},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {

			    	if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    		
			    
			  });

		}
		if(i>count){
			$('.log').append("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
			$('title').html("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
			clearInterval(start);
		}
		i++;
	}
	, time);


});
$('.btn-updateStatusBroadcast').click(function(){
	var status_broadcast = $('.status_brodcast').val()
	var status =  status_broadcast.split("|");

	
	// alert(status_random)

var row = []
	var user = []
	$('.list_status li').each(function(i,data){
		if($(this).find('.cb_user').prop('checked')){
			row.push($(this).find('.cb_user').data('id'));
			user.push($(this).find('.cb_user').data('user'));
			// $(this).fadeOut();
			}
	});
	var i=0;
	var count = row.length
	
	var status_fix = ""
	var ok = 0
	var error = 0
	var start = setInterval( function(){
	    // console.log(row[i]);
	    var status_random = status[Math.floor(Math.random() * status.length)];
	    status_fix = '@'+user[i]+' '+status_random
	    if(i<count){
	    	$.ajax({
					url :base+'/ajax/updateStatus',
					data:{status:status_fix},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {

			    	if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    		
			    
			  });

		}
		if(i>count){
			$('.log').append("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
			$('title').html("[Finish Broadcast] : Broadcast to "+ok+" Users<br>");
			clearInterval(start);
		}
		i++;
	}
	, time);
})


$('.btn-updateStory').click(function(){
	var	story = $('.story').val()
	var split = str_split(story, 130) 
	// console.log(split)
	var i=0;
	var count = split.length
	var ok = 0
	var error = 0
	var start = setInterval( function(){
	    // console.log(row[i]);
	    status_fix = '#'+(i+1)+' '+split[i]+"..."
	    if(i<count){
	    	$.ajax({
					url :base+'/ajax/updateStatus',
					data:{status:status_fix},
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			        }
			    }).done(function(data) {

			    	if(data.status=="ok"){
			    		ok = ok+1;
			    	}else{
			    		error = error+1;
			    	}
			    		$('.log').append(data.log);
			    		$('title').html(data.log);
			    		
			    
			  });
			// console.log(status_fix)

		}
		if(i>count){
			$('.log').append("[Finish Story] ");
			$('title').html("[Finish Story] ");
			$('.story').val("")
			clearInterval(start);
		}
		i++;
	}
	, time);

})
function str_split(string, split_length) {
  //  discuss at: http://phpjs.org/functions/str_split/
  // original by: Martijn Wieringa
  // improved by: Brett Zamir (http://brett-zamir.me)
  // bugfixed by: Onno Marsman
  //  revised by: Theriault
  //  revised by: Rafał Kukawski (http://blog.kukawski.pl/)
  //    input by: Bjorn Roesbeke (http://www.bjornroesbeke.be/)
  //   example 1: str_split('Hello Friend', 3);
  //   returns 1: ['Hel', 'lo ', 'Fri', 'end']

  if (split_length === null) {
    split_length = 1;
  }
  if (string === null || split_length < 1) {
    return false;
  }
  string += '';
  var chunks = [],
    pos = 0,
    len = string.length;
  while (pos < len) {
    chunks.push(string.slice(pos, pos += split_length));
  }

  return chunks;
}

function arrayCompare(a1, a2) {
    if (a1.length != a2.length) return false;
    var length = a2.length;
    for (var i = 0; i < length; i++) {
        if (a1[i] !== a2[i]) return false;
    }
    return true;
}

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}