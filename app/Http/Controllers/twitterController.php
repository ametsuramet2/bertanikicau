<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use \Twitter;
use App\Classes\CustomLib;

class twitterController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	private $count;

	public function __construct()
	{
		if(isset($_GET['count'])){
		 	$this->count = $_GET['count'];
		}else{
			$this->count = 50;
		}
	}

	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function show($screen)
	{
		$data = Twitter::getUsers(['screen_name'=>$screen]);
		if(isset($_GET['view'])){
			$status = Twitter::getHomeTimeline(['screen_name'=>$screen,'count' => $this->count]);
		}else{
			$status = Twitter::getUserTimeline(['screen_name'=>$screen,'count' => $this->count]);
		}
		return view('user',compact('data','status'));
		
	}
	public function mention($screen)
	{
		$data = Twitter::getCredentials();
		
		$status = Twitter::getMentionsTimeline(['count' => $this->count]);
		
		// print_r($status);
		return view('mention',compact('data','status'));
		
	}
	public function search(){
		$data = Twitter::getCredentials();
		$search = Twitter::getSearch(['q'=>$_GET['q'],'count' => $this->count]);
		return view('search',compact('data','search'));
	}
	public function follower($screen)
	{
		
		$data = Twitter::getUsers(['screen_name'=>$screen]);
		if(isset($_GET['cursor'])){
			$friend = Twitter::getFollowers(['screen_name'=>$screen,'count' => $this->count,'cursor'=>$_GET['cursor']]);
		}else{
			$friend = Twitter::getFollowers(['screen_name'=>$screen,'count' => $this->count]);
		}
		$data_friend =[];
		
		foreach ($friend->users as  $user) {
			
			$data_friend[] = $user->id;
		}

		$lookup =  Twitter::getFriendshipsLookup(['user_id'=>implode(',',$data_friend)]);
		return view('follower',compact('data','friend','lookup'));
	}

	public function following($screen)
	{
		$data = Twitter::getUsers(['screen_name'=>$screen]);
		if(isset($_GET['cursor'])){
			$friend = Twitter::getFriends(['screen_name'=>$screen,'count' => $this->count,'cursor'=>$_GET['cursor']]);
		}else{
			$friend = Twitter::getFriends(['screen_name'=>$screen,'count' => $this->count]);
		}
		$data_friend =[];
		
		foreach ($friend->users as  $user) {
			
			$data_friend[] = $user->id;
		}

		$lookup =  Twitter::getFriendshipsLookup(['user_id'=>implode(',',$data_friend)]);
		return view('follower',compact('data','friend','lookup'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
