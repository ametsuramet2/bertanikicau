<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use \Twitter;
use Response;

class ajaxController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}
	public function updateStatus()
	{
		// print_r(\Input::all());
		$status = \Input::get('status');
		// $update = 1;
		$status = substr($status,0,139);

			$update = Twitter::postTweet(['status' => $status]);
		if($update)
		{	

		    $Response = ['status'=>'ok','log'=>'[updated status]['.date('d-m-Y H:i:s').'] : '.$status.'<br />'];
		}
		else
		{
			
			$Response = ['status'=>'error','log'=>'<span style="color:red">[FAILED]</span>['.date('d-m-Y H:i:s').'] : '.$status.' <br />'];
		
		}


		return Response::json($Response);
	}
	public function DM()
	{
		// print_r(\Input::all());
		$status = \Input::get('status');
		$id = \Input::get('id');
		$user = \Input::get('user');

		// $update = 1;
		$status = substr($status,0,139);

		try
		{	
			$update = Twitter::postDm(['user_id'=>$id,'text' => $status]);

		    $Response = ['status'=>'ok','log'=>'[DM '.$user.']['.date('d-m-Y H:i:s').'] : '.$status.'<br />'];
		}
		catch (\Exception $e)
		{
			
			$Response = ['status'=>'error','log'=>'<span style="color:red">[FAILED]</span>['.date('d-m-Y H:i:s').'] : '.$status.' <br />'];
		
		}


		return Response::json($Response);
	}
	public function retweetStatus()
	{
		$id = \Input::get('id');
		$status = \Input::get('status');

		// $update = 1;
		
		try
		{
			$update = Twitter::postRt($id);

			$Response = ['status'=>'ok','log'=>'[Retweet status]['.date('d-m-Y H:i:s').'] : '.$status.'<br />'];
			
		}
		catch (\Exception $e)
		{
			$Response = ['status'=>'error','log'=>'<span style="color:red">[FAILED]</span>['.date('d-m-Y H:i:s').'] : '.$status.'Failed<br />'];
		
		}

	
		return Response::json($Response);

	}
	public function follow()
	{
		$id = \Input::get('id');
		$user = \Input::get('user');
		// $update = 1;

		try
		{
			$update = Twitter::postFollow(['user_id'=>$id]);

			$Response = ['status'=>'ok','log'=>'[Follow User]['.date('d-m-Y H:i:s').'] : @'.$user.'<br />'];
		}
		catch (\Exception $e)
		{
			$Response = ['status'=>'error','log'=>'<span style="color:red">[FAILED]</span>['.date('d-m-Y H:i:s').'] : follow @'.$user.'<br />'];
		}
		return Response::json($Response);

	}
	public function unfollow()
	{
		$id = \Input::get('id');
		$user = \Input::get('user');
		// $update = 1;

		try
		{
			$update = Twitter::postUnfollow(['user_id'=>$id]);

			$Response = ['status'=>'ok','log'=>'[Unfollow User]['.date('d-m-Y H:i:s').'] : @'.$user.'<br />'];
		}
		catch (\Exception $e)
		{
			$Response = ['status'=>'error','log'=>'<span style="color:red">[FAILED]</span>['.date('d-m-Y H:i:s').'] : unfollow @'.$user.'<br />'];
		}
		
		return Response::json($Response);

	}
	public function checkFriend()
	{
		$id = \Input::get('id');
		$lookup =  Twitter::getFriendshipsLookup(['user_id'=>$id]);

		$Response = ['status'=>'ok','id'=>$id,'lookup'=>$lookup[0]->connections];

		 return Response::json($Response);

	}

	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
