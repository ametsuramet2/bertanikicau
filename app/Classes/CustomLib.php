<?php 
namespace App\Classes;
use Carbon\Carbon as Carbon;
class CustomLib{

   public static function test() {

     
        return 'test';
    }

    public static  function autoURLtwitter($text){
		
		$reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

		$reg_hash = '/#+([a-zA-Z0-9_]+)/';

		if(preg_match($reg_exUrl, $text, $url)) {

		       $data = preg_replace($reg_exUrl, "<a  target='_blank' href=".$url[0].">".$url[0]."</a> ", $text);

		} else {

		       $data = $text;

		}

		$data = preg_replace("/@([A-Za-z0-9_]{1,15})/", "<a href='".url("/twitter/follower/$1")."' >$0</a> <a href='http://twitter.com/$1' target='_blank'><i class='fa  fa-external-link'></i></a>", $data);
		$data = preg_replace($reg_hash, "<a href='".url("/twitter/search?q=$1")."' >$0</a> <a href='http://twitter.com/search?q=%23$1' target='_blank'><i class='fa  fa-external-link'></i></a>", $data);

		return $data;
	}
   

	public static function difference($timestamp)
	{
		$data = \Twitter::ago($timestamp);
		return time()-strtotime($data);
	}
}